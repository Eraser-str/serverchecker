# -*- coding: utf-8 -*-

import urllib2
import time

import paramiko

from send_list import mails
from blanks import *
from models import MailSend
from config import MAIN_SERVER_IP, MAIN_SERVER_ROOT_PASS, MAIN_SERVER_USER


def ssh_server_up():
    """Функция пытается зайти на сервер под ssh и перезагрузить сервер api, используя ранее заготовленные .sh скрипты

    """
    path_to_sh = '/home/'  # Пусть к файлам stop.sh и run.sh
    api_run_sequence = 'cd ' + path_to_sh + '; sh restart_all.sh'  # Последовательность действий для перезагрузки

    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        client.connect(
            hostname=MAIN_SERVER_IP,
            username=MAIN_SERVER_USER,
            password=MAIN_SERVER_ROOT_PASS,
            port=22
        )
    except paramiko.BadAuthenticationType:  # Если нам не удается подключиться к серверу
        return "connect_error"
    stdin, stdout, stderr = client.exec_command(api_run_sequence)
    data = stdout.read() + stderr.read()
    print data
    client.close()
    # Если мы дошли до этого места, значит все нормально и можем сказать, что последовательность выполнена
    return "sequence_exec"


def mailing(subject, content):
    """Функция, рассылающая письма по адресам, указанным в списке mails.
    Использует метод general_mail из класса MailSend из модуля models.

    :param subject: тема письма
    :param content: cодержание писма

    :type subject: str
    :type content: str

    """
    print "=" * 15, time.time(), "=" * 15
    for mail in mails:
        MailSend().general_mail(mail, subject, content)
        print "send to " + mail
    return "done."


def server_checker():
    """Составляет uri для обращения к серверу и шлет запрос по этому uri.

    """
    uri = 'http://' + MAIN_SERVER_IP + '/api2.php'
    try:
        f = urllib2.urlopen(uri)
        return f.getcode()
    except Exception:  # Если сервер отвечает нам ошибкой, значит сервер апи не доступен. так и скажем
        return "not_available"


def main():
    """Вызывает вышеописанные функции и на основании полученного от них ответа.

    """
    # Попробуем связаться с сервером. Если все нормально, то просто проигнорируем обращение
    if server_checker() == "not_available":
        # Первым делом разошлем письма о том, что сервер недоступен, используя функцию mailing
        mailing(mail_templates['api_not_available']['subject'], mail_templates['api_not_available']['content'])
        # Если не можем подключиться к серверу по ssh - вышлем письмо с инструкцией
        if ssh_server_up() == "connect_error":
            mailing(mail_templates['server_down']['subject'], mail_templates['server_down']['content'])
        # Если последовательность для реабилитации выполнена успешно - проверим еще раз сервер на доступ из сети
        elif ssh_server_up() == "sequence_exec":
            # Получаем от сервера http код 200? хорошо, вышлем письмо, сообщающее о том, что скрипт справился сам
            if server_checker() != "not_available" and server_checker() == 200:
                mailing(mail_templates['server_up']['subject'], mail_templates['server_up']['content'])
            else:
                # TODO: решить что делать если север подымался функцией, но не доступен
                pass
        else:  # Если скрипт дошел сюда, значит я что-то упустил. сообщу об этом сам себе
            MailSend().general_mail(mails[2], "ScriptError", "server_checker")
    else:
        pass